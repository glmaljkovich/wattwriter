package ar.com.nextgendesign.wattwriter.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Window;
import android.widget.EditText;

import ar.com.nextgendesign.wattwriter.R;

/**
 * Created by gabriel on 30/08/16.
 */
public class AddFolderDialogFragment extends DialogFragment{

    EditText inputField;

    Listener activity;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        //dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.add_folder, null))
                // Add action buttons
                .setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        inputField = (EditText) getDialog().findViewById(R.id.enter_folder_name);
                        activity.onAddFolder(inputField.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        AddFolderDialogFragment.this.getDialog().cancel();
                    }
                });

        return builder.create();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.activity = (Listener) context;
    }

    public interface Listener{
        void onAddFolder(String name);
    }

}
