package ar.com.nextgendesign.wattwriter.utils;

import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.http.OkHttp3Requestor;
import com.dropbox.core.v2.DbxClientV2;

public class DropboxSession {
    private static final String APP_KEY     = "6209jhlc6x4tc47"; // Deprecated? Can't remember.
    private static final String APP_SECRET  = "mgtt899s5ugmzeu"; // Deprecated? Can't remember.
    private static DbxClientV2 sDbxClient;

    public static void init(String accessToken) {
        if (sDbxClient == null) {
            DbxRequestConfig requestConfig = DbxRequestConfig.newBuilder("WattWriter/1.0")
                    .withHttpRequestor(OkHttp3Requestor.INSTANCE)
                    .build();

            sDbxClient = new DbxClientV2(requestConfig, accessToken);
        }
    }

    public static DbxClientV2 getClient() {
        if (sDbxClient == null) {
            throw new IllegalStateException("Client not initialized.");
        }
        return sDbxClient;
    }

    public static boolean isCreated(){
        return sDbxClient != null;
    }

}
