package ar.com.nextgendesign.wattwriter.story;

import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.core.DbxDownloader;
import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.FolderMetadata;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.files.WriteMode;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import ar.com.nextgendesign.wattwriter.R;
import ar.com.nextgendesign.wattwriter.utils.DropboxSession;

import static ar.com.nextgendesign.wattwriter.db.StoriesDB.QueuedActions;


public class DropBoxStoryManager extends StoryManager {

    public DropBoxStoryManager(FragmentActivity act) {
        super(act);
    }


    public class SyncDropboxFilesTask extends AsyncTask<Void, Void, Void> {
        /** The system calls this to perform work in a worker thread and
         * delivers it the parameters given to AsyncTask.execute() */
        @Override
        protected Void doInBackground(Void... params) {
            //Retrieve the directory listing from dropbox
            //for each Entry in the list, get the filename and check if the last mod matches, then download
            try {
                ListFolderResult dbFolder = DropboxSession.getClient().files().listFolder("");
                syncRecursively(dbFolder);
            } catch (DbxException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class DeleteStoryFromDropboxTask extends AsyncTask<String, Void, Boolean> {
        /** The system calls this to perform work in a worker thread and
         * delivers it the parameters given to AsyncTask.execute() */
        @Override
        protected Boolean doInBackground(String... args) {
            String folder       = args[0];
            String storyName    = args[1];
            return deleteDropboxStory(folder + "/" + storyName);
        }
    }

    public class SaveStoryToDropboxTask extends AsyncTask<String, Void, Boolean> {
        /** The system calls this to perform work in a worker thread and
         * delivers it the parameters given to AsyncTask.execute() */
        @Override
        protected Boolean doInBackground(String... fileName) {
            return saveFileToDropbox(fileName[0]);
        }
    }

    public class PerformQueuedActionsTask extends AsyncTask<Void, Void, Void> {
        /** The system calls this to perform work in a worker thread and
         * delivers it the parameters given to AsyncTask.execute() */
        @Override
        protected Void doInBackground(Void... param) {
            deleteQueuedStories();
            saveQueuedStories();
            return null;
        }
    }

    /**
     * Recursively downloads the contents of a dropbox folder
     * */
    private void syncRecursively(ListFolderResult dbFolder) {
        // Iterate over the files
        for(Metadata entry : dbFolder.getEntries()){
            // if its a folder
            if(entry instanceof FolderMetadata){
                FolderMetadata folder = (FolderMetadata) entry;
                // create the folder
                new File(getStoriesDirectory().getPath() + entry.getPathDisplay()).mkdirs();
                try {
                    // recursively download its content
                    ListFolderResult folderContents = DropboxSession.getClient()
                            .files()
                            .listFolder(folder.getPathDisplay());
                    syncRecursively(folderContents);
                } catch (DbxException e) {
                    e.printStackTrace();
                }
            }
            // else download the file
            else{
                syncFileFromDropbox((FileMetadata) entry);
            }

        }
    }

    public void uploadFolderToDropbox(){
        // TODO
    }

    @NonNull
    @Override
    public File getStoriesDirectory() {
       return new File(this.activity.getExternalFilesDir(null).toString(), this.activity.getString(R.string.dropbox_folder_name));
    }

    @Override
    protected File getExportDirectory() {
        return new File(super.getStoriesDirectory(), "export");
    }

    /**
     * Saves a file to Dropbox
     * Runs on background
     * */
    @Override
    public void saveStory(Story story, File folder, int cursorPosition) {
        super.saveStory(story, folder, cursorPosition);
        // TODO make sure the task adds the .txt extension instead of receiving it
        new SaveStoryToDropboxTask().execute(folder.getPath() + "/" + story.getTitle() + ".txt");
    }
    /**
     * remove the storiesDirectory from path
     * PRECONDITION: The file is a folder contained within storiesDirectory.
     * */
    private String getRelativePath(File folder){
        return folder.getPath().replace(getStoriesDirectory().getPath(), "");
    }

    /**
     * Deletes a story from Dropbox
     * Deletes it's saved rev identifier
     * Runs on background
     * */
    @Override
    public void deleteStoryFrom(String storyName, File folder) {
        // remove the storiesDirectory from path
        String relativeFolder = getRelativePath(folder);
        // delete from dropbox
        new DeleteStoryFromDropboxTask().execute(relativeFolder, storyName);
        // delete file
        super.deleteStoryFrom(storyName, folder);
    }


    @Override
    public void deleteRecursively(File file) {
        // if it is a directory
        if (file.isDirectory()) {
            // delete its content from dropbox and the file system.
            for (File content : file.listFiles())
                deleteRecursively(content);
            // Then delete the directory from dropbox
            deleteDropboxFolder(file);
        }
        else // its a story so let's delete it from dropbox
            deleteDropboxStory(fileNameWithoutExtension(file.getPath()));
        // Finally, delete the file independently of its type.
        file.delete();
    }


    public boolean deleteDropboxFolder(File folder){
        Boolean deleted = true;
        try {
            DropboxSession.getClient().files().delete(getRelativePath(folder));
        } catch (DbxException e) {
            e.printStackTrace();
            Log.i("Error deleting DB file", folder.getPath() + " queued to be deleted");
            queueStoryToBeDeleted(folder.getPath());
            deleted = false;
        }
        return deleted;
    }

    /**
     * Deletes the story from dropbox and erases the entry from dropboxrev Table
     * Must run on an Async Task
     * @param storyPath without .txt extension
     * */
    @NonNull
    public Boolean deleteDropboxStory(String storyPath) {
        Boolean deleted = true;
        try {
            File file = new File(storyPath + ".txt");
            DropboxSession.getClient().files().delete(getRelativePath(file));
            storiesRepo.deleteFromDropboxRev(storyPath);
        } catch (DbxException e) {
            e.printStackTrace();
            Log.i("Error deleting DB file", storyPath + " queued to be deleted");
            queueStoryToBeDeleted(storyPath);
            deleted = false;
        }
        return deleted;
    }

    /**
     * Tries to save a file to Dropbox. Queue's it to be saved if there's no internet connection.
     * Must run on an Async Task
     * @param filePath with filename and extension
     * */
    public Boolean saveFileToDropbox(String filePath) {
        File file       = new File(filePath);
        Boolean saved   = true;
        FileInputStream inputStream;
        String relativePath = getRelativePath(file);
        try {
            inputStream = new FileInputStream(file);
            try {
                DropboxSession.getClient().files().uploadBuilder(relativePath).withMode(WriteMode.OVERWRITE).uploadAndFinish(inputStream);
            } catch (DbxException | IOException e) {
                Log.i("Error saving DB file", filePath + " queued to be saved");
                e.printStackTrace();
                queueStoryToBeSaved(filePath.substring(0, filePath.lastIndexOf(".")));
                saved = false;
            }
        } catch (FileNotFoundException e) {
            Log.e("Error on Input Stream", "Stack trace below");
            e.printStackTrace();
            saved = false;
        }
        return saved;
    }

    /**
     * Downloads an entry into the fileSystem if it's revision is newer than the local copy.
     * Must run on an Async Task
     * */
    protected void syncFileFromDropbox(FileMetadata entry) {
        // TODO get cased file names
        // Construct the file
        File file        = new File(getStoriesDirectory().getPath() + entry.getPathDisplay());
        String storyPath = fileNameWithoutExtension(entry.getPathDisplay());
        // Check if the file exists in the client device
        if(file.exists()){
            // if the revisions don't match
            if(!entry.getRev().equals(getStoryRevision(storyPath))){
                downloadFromDropbox(entry, file);
            }
        }
        else{
            downloadFromDropbox(entry, file);
        }
    }

    /**
     * Must run on an AsyncTask
     * @return a revision for the story if it has one. Otherwise, a default value is returned.
     * */
    private String getStoryRevision(String storyPath) {
        return storiesRepo.getStoryRevision(storyPath);
    }

    /**
     * Downloads a dropbox entry into a local file.
     * Must run on an Async Task
     * */
    protected void downloadFromDropbox(FileMetadata entry, File file){
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            // Saves the ouputStream to the local file
            DbxDownloader downloader = DropboxSession.getClient().files().download(entry.getPathDisplay());
            downloader.download(outputStream);
            Log.i("DB file path", entry.getPathDisplay());
        } catch (Exception e) {
            Log.e("Dropbox error","Something went wrong: " + e);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                    String storyPath = fileNameWithoutExtension(entry.getPathDisplay());
                    storiesRepo.saveDropboxRev(storyPath, entry);
                } catch (IOException e) {Log.e("Dropbox error","Couldn't close outputStream: " + e);}
            }
        }
    }

    public void syncDropboxFiles() {
        //Retrieve the directory listing from dropbox
        //for each Entry in the list, get the filename and check if the last mod matches, then download
        new SyncDropboxFilesTask().execute();
    }

    public void performQueuedActions(){
        new PerformQueuedActionsTask().execute();
    }

    private void queueStoryToBeSaved(String storyName) {
        storiesRepo.queueStoryAction(storyName, QueuedActions.ACTION_SAVE);
    }

    private void queueStoryToBeDeleted(String storyName) {
        storiesRepo.queueStoryAction(storyName, QueuedActions.ACTION_DELETE);
    }

    /**
     * Must run on an Async Task
     * */
    public void deleteQueuedStories(){
        storiesRepo.deleteQueuedStories(this);
    }

    /**
     * Must run on an Async Task
     * */
    public void saveQueuedStories(){
        storiesRepo.saveQueuedStories(this);
    }


}
