package ar.com.nextgendesign.wattwriter.story;


public class Folder extends GridElement {

    public Folder(String title) {
        this.title = title;
    }

    @Override
    public void open(GridElement.Listener listener) {
        listener.openFolder(title, false);
    }

    @Override
    public void delete(Listener listener) {
        listener.deleteFolder(this.title);
    }

    @Override
    public int getType() {
        return 1;
    }
}
