package ar.com.nextgendesign.wattwriter.html;


public class StyleDef {
    private int resourceID;
    private String styleName;
    private HTMLStyle stilo;

    public StyleDef(String name, int id, HTMLStyle style){
        this.resourceID = id;
        this.styleName  = name;
        this.stilo      = style;
    }

    public int getResourceID() {
        return resourceID;
    }

    public String getStyleName() {
        return styleName;
    }

    public HTMLStyle getStyle(){ return stilo;}

}
