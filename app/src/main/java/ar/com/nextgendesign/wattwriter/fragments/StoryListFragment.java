package ar.com.nextgendesign.wattwriter.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.core.android.Auth;

import java.io.File;
import java.util.ArrayList;

import ar.com.nextgendesign.wattwriter.R;
import ar.com.nextgendesign.wattwriter.activities.MainActivity;
import ar.com.nextgendesign.wattwriter.activities.StoryEditorActivity;
import ar.com.nextgendesign.wattwriter.story.Folder;
import ar.com.nextgendesign.wattwriter.story.GridElement;
import ar.com.nextgendesign.wattwriter.story.Story;
import ar.com.nextgendesign.wattwriter.story.StoryAdapter;
import ar.com.nextgendesign.wattwriter.story.StoryManager;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class StoryListFragment extends Fragment implements GridElement.Listener {
    protected GridView storyGrid;
    protected StoryManager storyManager;
    protected StoryAdapter storyAdapter;
    ArrayList<GridElement> storiesArray;
    private SwipeRefreshLayout mainView;
    private TextView breadcrums;
    public Context contexto;
    protected String LIST_TYPE;
    protected String LAST_FOLDER;
    protected AsyncTask runningTask;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set as last opened Fragment
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        sharedPref.edit().putString(getString(R.string.LastStoryList), LIST_TYPE).apply();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.content_main, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        // Set the title on the action bar
        setToolbarTitle();
        // Set the SwipeRefreshLayout
        mainView = (SwipeRefreshLayout) activity.findViewById(R.id.main_view);
/*        ViewCompat.setNestedScrollingEnabled(mainView,true);*/
        mainView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(storiesArray != null){
                    populateAdapter();
                }
                else {
                    showStories();
                }
            }
        });
        // Configure the refreshing colors
        mainView.setColorSchemeResources(android.R.color.holo_blue_bright,
                R.color.colorAccent,
                R.color.colorPrimary,
                android.R.color.holo_orange_light);
        this.showStories();
        this.updateBreadCrums();
    }

    @Override
    public void onResume() {
        super.onResume();
        this.showStories();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        contexto = context;
    }

    protected void setToolbarTitle(){
        // Set the title on the action bar
        MainActivity activity = (MainActivity) getActivity();
        activity.setTitle(this.getTitle());
    }

    /**
     * Returns the string to be used to define the storyManager
     * */
    public abstract String storySource();

    /**
     * Shows the stories in the UI, calling createGridOfStories()
     * */
    public abstract void showStories();

    /**
     * Returns the title to be shown on the toolbar
     * @return The current folder's name
     * */
    public String getTitle(){
        return parseFolderName(getLastOpenedFolder());
    };

    /**
     * returns the current Fragment's item id in the drawer
     * @return a valid Drawer menu item id
     * */
    public abstract long getItemID();

    public void cancelRunningTask() {
        if(this.runningTask != null){
            this.runningTask.cancel(true);
            Log.e("Task CANCEL", this.runningTask.toString());
        }

    }

    /**
     * Updates the Stories in the Adapter
     * Notifies the SwipeRefreshLayout when it finished loading
     * */
    public class PopulateAdapterTask extends AsyncTask<String, Void, ArrayList<GridElement>>{
        @Override
        protected void onPreExecute() {
            // Show Loading Spinner
            if(!mainView.isRefreshing()){
                mainView.post(new Runnable() {
                    @Override
                    public void run() {
                        mainView.setRefreshing(true);
                    }
                });
            }
            Log.e("TASK START", this.toString());
            super.onPreExecute();
        }

        @Override
        protected ArrayList<GridElement> doInBackground(String... params) {
            if(!this.isCancelled()){
                File folder = new File(getHomeDirectory(), params[0]);
                return storyManager.getGridElements(folder);
            }
            else{
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<GridElement> stories) {
            storiesArray.clear();
            storiesArray.addAll(stories);
            storyAdapter.setStoryArray(storiesArray);
            storyAdapter.notifyDataSetChanged();

            toggleWelcomeScreenVisibility(storiesArray);

            mainView.setRefreshing(false);
            runningTask = null;
            Log.e("TASK END", this.toString());
        }

        @Override
        protected void onCancelled() {
            Log.i("OnCancelled", "llamado");
            mainView.setRefreshing(false);
            runningTask = null;
            super.onCancelled();
        }
    }

    public void showContent(File storiesFolder) {
        // if Folder is not empty
        if(storiesFolder.list().length > 0){
            createGridOfStories();
        }
        else{
            toggleWelcomeScreenVisibility(storiesArray);
        }
    }

    /**
     * Changes the BreadCrums Navigation text with the lastOpenedFolder
     * */
    public void updateBreadCrums(){
        breadcrums = (TextView) this.getActivity().findViewById(R.id.breadcrums);
        breadcrums.setText(getLastOpenedFolder());
    }

    /**
     * Shows the welcome screen if there are no stories.
     * @param storiesArray the storiesArray
     * */
    protected void toggleWelcomeScreenVisibility(ArrayList<GridElement> storiesArray) {
        NestedScrollView homeScreen = (NestedScrollView) getActivity().findViewById(R.id.homeScreenContainer);
        storyGrid = (GridView) this.getActivity().findViewById(R.id.theGrid);

        if(storiesArray.isEmpty()){
            homeScreen.setVisibility(View.VISIBLE);
            storyGrid.setVisibility(View.GONE);
        }else {
            homeScreen.setVisibility(View.GONE);
            storyGrid.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Updates the adapter with the new Stories
     * */
    public void populateAdapter(){
        if(runningTask == null){
            this.runningTask = new PopulateAdapterTask().execute(getLastOpenedFolder());
        }

    }

    /**
     * It populates the story grid with the stories in the files's array through an adapter.
     * */
    protected void createGridOfStories() {
        storiesArray = new ArrayList<>();

        // Create the Grid
        storyGrid = (GridView) this.getActivity().findViewById(R.id.theGrid);
        ViewCompat.setNestedScrollingEnabled(storyGrid,true);

        // Set the Adapter
        storyAdapter = new StoryAdapter(this.getActivity(), storiesArray);
        storyGrid.setAdapter(storyAdapter);

        storyGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> grid, View view, int position, long id) {

                GridElement item = (GridElement) grid.getItemAtPosition(position);

                item.open(StoryListFragment.this);
            }
        });

        // Handle context menu actions
        storyGrid.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);
        storyGrid.setMultiChoiceModeListener(new StoryListMultiChoiceModeListener());
        storyGrid.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition =
                        (storyGrid == null || storyGrid.getChildCount() == 0) ?
                                0 : storyGrid.getChildAt(0).getTop();
                mainView.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);
            }
        });

        populateAdapter();
    }

    /**
     * Open a particular story to be edited.
     * */
    public void openStory(String storyTitle) {
        // Get the text
        String body = this.storyManager.openStory(storyTitle, getCurrentFolder());
        // Build the intent
        Intent intent = new Intent(this.getActivity(),StoryEditorActivity.class);
        intent.putExtra(MainActivity.STORY_TITLE, storyTitle);
        intent.putExtra(MainActivity.STORY_CONTENT, body);
        intent.putExtra(MainActivity.STORY_SOURCE, storySource());
        intent.putExtra(MainActivity.LAST_OPENED_FOLDER, getLastOpenedFolder());
        // Start the activity
        startActivity(intent);
    }

    @NonNull
    protected File getCurrentFolder() {
        return new File(getHomeDirectory(), getLastOpenedFolder());
    }

    public void addFolder(String name){
        File newFolder = new File(getCurrentFolder(), name);
        newFolder.mkdir();
        showStories();
    }

    /**
     * Reloads the fragment with the specified folder
     * @param isAbsolutePath indicates if the folder is inside the lastOpenedFolder() or not.
     * */
    @Override
    public void openFolder(String folderName, boolean isAbsolutePath){

        saveLastOpenedFolder(folderName, isAbsolutePath);

        setToolbarTitle();

        createGridOfStories();

        getActivity().invalidateOptionsMenu();

        updateBreadCrums();
    }

    /**
     * Cleans a folder path to get just the last folder.
     * */
    private String parseFolderName(String path){
        String folderName;
        // if we are in the home directory
        if(path.equals("")){
            folderName = getHomeFolderName();
        } else{
            // Separate folders
            String[] folders = path.split("/");
            // get last or return the default path if there were no slashes
            folderName = (folders.length > 0) ? folders[folders.length - 1] : path;
        }
        return folderName;
    }

    protected abstract String getHomeFolderName();

    /**
     * Saves the @folderName as the last opened folder
     * @param folderName the folder's title.
     * @param override indicates if the previous folder hierarchy should be discarded
     *                 and replaced by the folder passed as parameter.
     * */
    public void saveLastOpenedFolder(String folderName, boolean override) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String newPath = folderName;
        if(!override){
            newPath = getLastOpenedFolder() + "/" + folderName;
        }
        sharedPref.edit().putString(LAST_FOLDER, newPath).apply();
    }

    /**
     * Goes up one level in the folder hierarchy
     * PRECONDITION: The user isn't in the home directory
     * */
    public void navigateUp(){
        String previous = "";
        String current = getLastOpenedFolder();
        int lastSlash = current.lastIndexOf("/");
        if(lastSlash >=0){
            previous = current.substring(0, current.lastIndexOf("/"));
        }
        openFolder(previous, true);

    }

    /**
     * Indicates if the user can go up one more level in the folder hierarchy
     * */
    public boolean canNavigateUp(){
        return !getLastOpenedFolder().equals("");
    }

    public String getLastOpenedFolder() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(contexto);
        return sharedPref.getString(LAST_FOLDER, "");
    }

    public File getHomeDirectory() {
        return this.storyManager.getStoriesDirectory();
    }

    /**
     * Deletes a story from the current folder
     * */
    @Override
    public void deleteStory(String title){
        //Delete the story
        //TODO put this in an AsyncTask
        this.storyManager.deleteStoryFrom(title, getCurrentFolder());
    }

    /**
     * Used when the specified file is going to be deleted directly without
     * relating its path to the currentFolder()
     * */
    public void deleteFolderWithAbsolutePath(File folder){
        this.storyManager.deleteFolder(folder);
    }

    /**
     * Deletes a folder inside the current directory.
     * */
    @Override
    public void deleteFolder(String title) {
        File folder = new File(getCurrentFolder(), title);
        this.storyManager.deleteFolder(folder);
    }

    /**
     * Deletes the selected stories from the grid
     * @param item the CAB button that called this method
     * */
    public void deleteSelectedItems(MenuItem item) {
        //Get all the checked items (In this case our IDs are dynamic and we can't use them)
        SparseBooleanArray checkedItems = storyGrid.getCheckedItemPositions();
        //Just in case check if they are selected
        ArrayList<GridElement> elementsToBeDeleted = new ArrayList<>();
        for (int i=0; i < checkedItems.size(); i++){
            if(checkedItems.valueAt(i)){
                //Delete the story
                GridElement element = (GridElement) storyGrid.getItemAtPosition(checkedItems.keyAt(i));
                //TODO put this in an AsyncTask
                element.delete(this);
                //add to the remove list
                elementsToBeDeleted.add(element);
            }
        }
        // Once all are identified, delete them
        for(GridElement e : elementsToBeDeleted){
            storiesArray.remove(e);
        }
        //Notify on complete
        Toast.makeText(this.getActivity(), R.string.files_deleted, Toast.LENGTH_SHORT).show();
        // Update the grid
        storyAdapter.notifyDataSetChanged();
    }


    private void showDeleteItemsConfirmationDialog(final ActionMode mode, final MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.delete_selected_items);
        builder.setMessage(R.string.delete_items_confirmation_request);
        builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteSelectedItems(item);
                mode.finish(); // Action picked, so close the CAB
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Do nothing
            }
        });
        builder.setCancelable(true);
        AlertDialog deleteDialog = builder.create();
        deleteDialog.show();
    }


    public class StoryListMultiChoiceModeListener implements AbsListView.MultiChoiceModeListener {

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position,
        long id, boolean checked) {
            // Update the title in the CAB
            mode.setTitle(storyGrid.getCheckedItemCount() + " " + getString(R.string.selected_items));
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            // Respond to clicks on the actions in the CAB
            switch (item.getItemId()) {
                case R.id.menu_delete:
                    showDeleteItemsConfirmationDialog(mode, item);
                    return true;
                case R.id.move:
                    triggerMoveFilesMode();
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate the menu for the CAB
            MenuInflater inflater = new MenuInflater(contexto);
            inflater.inflate(R.menu.context_menu, menu);
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            // Here you can make any necessary updates to the activity when
            // the CAB is removed. By default, selected items are deselected/unchecked.
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            // Here you can perform updates to the CAB due to
            // an invalidate() request
            return false;
        }
    }

    /**
     * Opens the bottom sheet for moving files.
     * */
    private void triggerMoveFilesMode() {
        Toast.makeText(this.getContext(), "Not implemented yet!", Toast.LENGTH_SHORT).show();
    }

}