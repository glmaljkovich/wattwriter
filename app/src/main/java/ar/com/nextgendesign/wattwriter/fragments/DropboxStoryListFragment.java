package ar.com.nextgendesign.wattwriter.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.TextView;

import com.dropbox.core.android.Auth;

import java.io.File;
import java.util.ArrayList;

import ar.com.nextgendesign.wattwriter.R;
import ar.com.nextgendesign.wattwriter.activities.MainActivity;
import ar.com.nextgendesign.wattwriter.activities.StoryEditorActivity;
import ar.com.nextgendesign.wattwriter.story.DropBoxStoryManager;
import ar.com.nextgendesign.wattwriter.utils.DropboxSession;

/**
 * A simple {@link Fragment} subclass.
 */
public class DropboxStoryListFragment extends StoryListFragment {
    boolean sessionCreated = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        LIST_TYPE = "dropbox";
        LAST_FOLDER = "LAST_OPENED_DROPBOX";
        super.onCreate(savedInstanceState);

        authorizeSession(getToken());
        this.storyManager = new DropBoxStoryManager(this.getActivity());
    }

    @Override
    public void onResume() {
        if(getToken() == null){
            String token = Auth.getOAuth2Token();
            authorizeSession(token);
        }

        super.onResume();
    }

    private void authorizeSession(String token) {
        if (token == null) {
            // Initialize Authorization process
            Log.e("authorizeSession", "autenticando");
            showDropboxLoginDialog();

        }
        else {
            // Initialize Dropbox API
            saveToken(token);
            DropboxSession.init(token);
        }
    }

    private void showDropboxLoginDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.login_to_dropbox);
        builder.setMessage(R.string.dropbox_login_explanation);
        builder.setPositiveButton(R.string.login, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Auth.startOAuth2Authentication(getContext(), "6209jhlc6x4tc47");
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //Do nothing
            }
        });
        builder.setCancelable(true);
        AlertDialog loginDialog = builder.create();
        loginDialog.show();
    }


    private String getToken() {
        // Get Token
        return getActivity().getApplicationContext()
                .getSharedPreferences("DB_PREF", Context.MODE_PRIVATE)
                .getString("DB_TOKEN", null);
    }

    private void saveToken(String token) {
        // This private shared preference should be available to all activities
        SharedPreferences.Editor editor = getActivity().getApplicationContext().getSharedPreferences("DB_PREF", Context.MODE_PRIVATE).edit();
        editor.putString("DB_TOKEN", token).apply();
    }


    @Override
    public String storySource() {
        return "dropbox";
    }


    @Override
    public void showStories() {
        if(isSessionCreated()){

            this.storyManager = new DropBoxStoryManager(this.getActivity());
            storiesArray = new ArrayList<>();

            // Makes the story folder
            File storiesFolder = this.storyManager.makeStoryFolder();
            // if Folder exists, then
            if(storiesFolder.exists()){
                DropBoxStoryManager myManager = (DropBoxStoryManager) this.storyManager;
                myManager.performQueuedActions();
                myManager.syncDropboxFiles();
                showContent(storiesFolder);

            }
            else { // if can't write to storage
                //Show error to user
                TextView subtitle = (TextView) this.getActivity().findViewById(R.id.subtitle);
                subtitle.setText(getString(R.string.errorNoSDCard));
                Log.e("Error Grave", "No se pudo crear el directorio de historias");
            }

        }
    }

    private boolean isSessionCreated(){
        return DropboxSession.isCreated();
    }

    @Override
    public void populateAdapter() {
        DropBoxStoryManager myManager = (DropBoxStoryManager) this.storyManager;
        myManager.performQueuedActions();
        myManager.syncDropboxFiles();
        super.populateAdapter();
    }

    @Override
    protected String getHomeFolderName() {
        return getString(R.string.dropbox);
    }

    @Override
    public long getItemID() {
        return R.id.dropbox;
    }


}
