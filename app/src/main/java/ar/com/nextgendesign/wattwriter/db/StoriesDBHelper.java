package ar.com.nextgendesign.wattwriter.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static ar.com.nextgendesign.wattwriter.db.StoriesDB.CursorPosition;
import static ar.com.nextgendesign.wattwriter.db.StoriesDB.DropboxRev;
import static ar.com.nextgendesign.wattwriter.db.StoriesDB.QueuedActions;

public class StoriesDBHelper extends SQLiteOpenHelper {
    public static final String DB_NAME       = "storiesDB.db";
    public static final int DATABASE_VERSION = 5;

    public StoriesDBHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(QueuedActions.CREATE_TABLE);
        db.execSQL(DropboxRev.CREATE_TABLE);
        db.execSQL(CursorPosition.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(newVersion > oldVersion){
            Log.e("onUpgrade", "executed");
            db.execSQL(QueuedActions.DELETE_TABLE);
            db.execSQL(DropboxRev.DELETE_TABLE);
            db.execSQL(CursorPosition.DELETE_TABLE);
            this.onCreate(db);
        }
    }

    @Override
    protected void finalize() throws Throwable {
        this.close();
        super.finalize();
    }
}
