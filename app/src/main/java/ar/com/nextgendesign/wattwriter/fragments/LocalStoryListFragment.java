package ar.com.nextgendesign.wattwriter.fragments;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import ar.com.nextgendesign.wattwriter.R;
import ar.com.nextgendesign.wattwriter.activities.MainActivity;
import ar.com.nextgendesign.wattwriter.story.StoryManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocalStoryListFragment extends StoryListFragment {

    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 255;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        LIST_TYPE   = "local";
        LAST_FOLDER = "LAST_OPENED_LOCAL";
        super.onCreate(savedInstanceState);
        // Story Manager
        this.storyManager = new StoryManager(this.getActivity());
    }

    /**
     * Used when opening a story in the editor to define what kind of storyManager should be used.
     * */
    @Override
    public String storySource() {
        return "local";
    }

    /**
     * Shows the stories in the Stories folder.
     * */
    public void showStories() {
        storiesArray = new ArrayList<>();
        // For Android 6.0 and up
        if(checkFolderPermission()){
            // Internal or external folder result
            File storiesFolder = this.storyManager.makeStoryFolder();
            // if Folder exists, then
            if(storiesFolder.exists()){
                showContent(storiesFolder);
            }
            else{ // if can't write to storage
                //Show error to user
                TextView subtitle = (TextView) this.getActivity().findViewById(R.id.subtitle);
                subtitle.setText(getString(R.string.errorNoSDCard));
                Log.e("Error Grave", "No se pudo crear el directorio de historias");
            }
        }

    }

    /**
     * Checks the Write external storage permission in devices running Android 6.0 and over
     * */
    public boolean checkFolderPermission(){
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                Toast.makeText(getContext(), "Need to save your stories in the phone. Could you let me, pretty pleaaase? (*.*)",
                        Toast.LENGTH_LONG).show();
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            }
            else {

                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
            return false;
        }
        else {
            return true;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay!
                    showStories();

                }
            }
        }
    }

    @Override
    public long getItemID() {
        return R.id.story_list;
    }

    @Override
    protected String getHomeFolderName() {
        return getString(R.string.stories_folder_name);
    }


}
