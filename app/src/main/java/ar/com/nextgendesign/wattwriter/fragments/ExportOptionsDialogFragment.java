package ar.com.nextgendesign.wattwriter.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ar.com.nextgendesign.wattwriter.R;

/**
 * Created by gabriel on 18/06/16.
 */
public class ExportOptionsDialogFragment extends BottomSheetDialogFragment {
    private Exporter exporter;

    public static ExportOptionsDialogFragment newInstance() {
        return new ExportOptionsDialogFragment();
    }

    public ExportOptionsDialogFragment setExporter(Exporter exporter){
        this.exporter = exporter;
        return this;
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.export_options, null);
        dialog.setContentView(contentView);

        final ExportOptionsDialogFragment bs = this;

        Button exportTXT = (Button) contentView.findViewById(R.id.export_as_TEXT_btn);
        Button exportHTML = (Button) contentView.findViewById(R.id.export_as_HTML_btn);

        exportTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exporter.exportAsTEXT();
                bs.dismiss();
            }
        });

        exportHTML.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exporter.exportAsHTML();
                bs.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if( behavior != null && behavior instanceof BottomSheetBehavior ) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }
    }

    public interface Exporter{
        void exportAsHTML();

        void exportAsTEXT();
    }


}
