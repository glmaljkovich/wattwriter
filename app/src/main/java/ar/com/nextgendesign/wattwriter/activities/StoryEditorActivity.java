package ar.com.nextgendesign.wattwriter.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import ar.com.nextgendesign.wattwriter.R;
import ar.com.nextgendesign.wattwriter.fragments.ChooseHTMLStyleDialogFragment;
import ar.com.nextgendesign.wattwriter.fragments.ExportOptionsDialogFragment;
import ar.com.nextgendesign.wattwriter.html.HTMLStyle;
import ar.com.nextgendesign.wattwriter.html.HTMLTemplate;
import ar.com.nextgendesign.wattwriter.story.DropBoxStoryManager;
import ar.com.nextgendesign.wattwriter.story.Story;
import ar.com.nextgendesign.wattwriter.story.StoryManager;
import ar.com.nextgendesign.wattwriter.utils.DropboxSession;
import ar.com.nextgendesign.wattwriter.utils.TextViewUndoRedo;
/**
 * The UI for the Story Editor
 * */
public class StoryEditorActivity extends AppCompatActivity implements ChooseHTMLStyleDialogFragment.DialogListener,
TextViewUndoRedo.UIListener, ExportOptionsDialogFragment.Exporter{

    private EditText title;
    private EditText texto;
    private Toolbar toolbar;
    private boolean saved;
    SharedPreferences sharedPref;
    private StoryManager storyManager;
    private int cursorPosition;
    private TextViewUndoRedo textHistory;
    private boolean undoButtonIsOff;
    private boolean redoButtonIsOff;
    private boolean toolbarSaveButtonVisible;
    private boolean textJustCreated;
    private int maxTextSize;
    private String LAST_FOLDER;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_editor);

        // Initialize the toolbar
        initializeToolbar();

        // Get the preferences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        // Initialize Text fields
        title   = (EditText) findViewById(R.id.title);
        texto   = (EditText) findViewById(R.id.texto);

        Typeface openSans = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        texto.setTypeface(openSans);

        //Set the Theme
        setDefaultEditorTheme();

        // UndoRedo ////////////////////////////////////////////////
        undoButtonIsOff = true;
        redoButtonIsOff = true;
        toolbarSaveButtonVisible = true;
        textJustCreated = true;

        // Initialize textHistory
        textHistory = new TextViewUndoRedo(texto);
        textHistory.setUIListener(this);

        //////////////////////////////////////////////////////////

        // Get Intent Data
        Intent intent       = getIntent();
        String storyTitle   = intent.getStringExtra(MainActivity.STORY_TITLE);
        String storyContent = intent.getStringExtra(MainActivity.STORY_CONTENT);
        String storySource  = intent.getStringExtra(MainActivity.STORY_SOURCE);
        LAST_FOLDER         = intent.getStringExtra(MainActivity.LAST_OPENED_FOLDER);

        // Set text size because of korean phones with that specific problem
        setMaxTextSize(9999999);

        // Set the Story values
        title.setText(storyTitle, TextView.BufferType.EDITABLE);
        texto.setText(storyContent, TextView.BufferType.EDITABLE);


        // Set the storyManager
        setTheStoryManager(storySource);

        // Initialize the save button
        initializeSavebutton(savedInstanceState);
        NestedScrollView scrollview = (NestedScrollView) findViewById(R.id.scrollView);

        scrollview.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(saveButtonIsVisible() && toolbarSaveButtonVisible){
                    toggleSaveToolbarButtonVisibility();
                }else if(!saveButtonIsVisible() && !toolbarSaveButtonVisible){
                    toggleSaveToolbarButtonVisibility();
                }
            }
        });

    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        //get cursor position
        super.onPostCreate(savedInstanceState);
        updateWordCount(texto.getText());
        restoreSavedCursorPosition(getCurrentFolder().getPath() + "/" + title.getText().toString());
    }

    /**
     * Sets the cursor on the place where the user last used it.
     * */
    private void restoreSavedCursorPosition(String storyPath) {
        cursorPosition = storyManager.getCursorPosition(storyPath);
        if(cursorPosition > 0 && cursorPosition < texto.length()){
            texto.setFocusableInTouchMode(true);
            texto.requestFocus();
            texto.setSelection(cursorPosition);
        }
    }

    private void initializeSavebutton(Bundle savedInstanceState) {
        // Initialize the save button
        if(savedInstanceState == null){
            saved = true;
            findViewById(R.id.fab).setAlpha(0.5f);
        }
    }

    private boolean saveButtonIsVisible(){
        View fab = findViewById(R.id.fab);
        Rect scrollBounds = new Rect();
        findViewById(R.id.scrollContainer).getHitRect(scrollBounds);
        return fab.getLocalVisibleRect(scrollBounds);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_story_editor, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem undo       = menu.findItem(R.id.undo);
        MenuItem redo       = menu.findItem(R.id.redo);
        MenuItem dayMode    = menu.findItem(R.id.day_mode);
        MenuItem nightMode  = menu.findItem(R.id.night_mode);
        MenuItem save       = menu.findItem(R.id.toolbar_save);

        // handle editorTheme
        String themeMode = sharedPref.getString("THEME", "day");
        if(themeMode.equals("night")){
            switchButtonsVisibility(dayMode, nightMode);
        }else{
            switchButtonsVisibility(nightMode, dayMode);
        }

        //Toggle save button visibility on toolbar
        save.setVisible(!saveButtonIsVisible());
        save.setEnabled(!saveButtonIsVisible());

        if(!isSaved()){
            save.getIcon().setAlpha(255);
        }else{
            save.getIcon().setAlpha(100);
        }

        // handle undoButton
        handleButton(undo, undoButtonIsOff(), R.drawable.undo_black);

        // handle redoButton
        handleButton(redo, redoButtonIsOff(), R.drawable.redo_black);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.undo:
                undo();
                return true;

            case R.id.redo:
                redo();
                return true;

            case R.id.toolbar_save:
                saveStoryOnBackground();
                toggleSaveToolbarButtonVisibility();
                findViewById(R.id.fab).setAlpha(0.5f);
                return true;

            case R.id.export:
                // export
                openExportOptions(item);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    private void toggleSaveToolbarButtonVisibility() {
        toolbarSaveButtonVisible = !toolbarSaveButtonVisible;
        invalidateOptionsMenu();
    }

    @Override
    public void onBackPressed() {
        if(autoSaveIsOn() && !isSaved()){
            saveStoryOnBackground();
        }
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        if(!isChangingConfigurations() && autoSaveIsOn() && !isSaved()){
            saveStoryOnBackground();
            //Notify on Complete
            //Toast.makeText(this, R.string.saved_story_notification, Toast.LENGTH_SHORT).show();
        }
        super.onStop();
    }

    /**
     * Handles buttons in the toolbar
     * @param drawable the black version of the icon
     * */
    private void handleButton(MenuItem button, boolean isOff, int drawable){
        if(!isOff){
            button.setEnabled(true);
            button.getIcon().setAlpha(255);
        }else{
            button.setIcon(drawable);
            button.getIcon().setAlpha(65);
        }
    }

    /**
     * Sets the Theme loaded on start up
     * */
    private void setDefaultEditorTheme() {
        String themeMode = sharedPref.getString("THEME", "day");
        if(themeMode.equals("night")){
            setNightEditorTheme(null);
        }else{
            setDayEditorTheme(null);
        }
    }

    public void setDayEditorTheme(@Nullable MenuItem item) {
        // if was called from the UI
        if(item != null){
            sharedPref.edit().putString("THEME","day").apply();
        }
        setEditorThemeWithColors("#ffffff", "#000000");
    }

    public void setNightEditorTheme(@Nullable MenuItem item) {
        // if was called from the UI
        if(item != null){
            sharedPref.edit().putString("THEME","night").apply();
        }
        setEditorThemeWithColors("#222222", "#ffffff");
    }

    /**
     * Changes the color scheme of the editor
     * */
    private void setEditorThemeWithColors(String backgroundColor, String textColor){
        // Set Background color
        View editorMainContainer = findViewById(R.id.editor_main_container);
        editorMainContainer.setBackgroundColor(Color.parseColor(backgroundColor));
        texto.setBackgroundColor(Color.parseColor(backgroundColor));
        // Set text color
        title.setBackgroundColor(Color.parseColor(backgroundColor));
        texto.setTextColor(Color.parseColor(textColor));
        invalidateOptionsMenu();
    }

    private void initializeToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextAppearance(this, R.style.Text_Editor_toolbar_text);
        toolbar.setTitle("hola");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setTheStoryManager(String storySource) {
        if(storySource.equals("local")){
            this.storyManager = new StoryManager(this);
        }
        else {
            // Get Token
            String token = getApplicationContext().getSharedPreferences("DB_PREF", Context.MODE_PRIVATE).getString("DB_TOKEN", "not set");
            DropboxSession.init(token);
            // Set the DropboxStoryManager
            this.storyManager = new DropBoxStoryManager(this);
        }
    }


    public boolean undoButtonIsOff() {
        return this.undoButtonIsOff;
    }

    public boolean redoButtonIsOff() {
        return this.redoButtonIsOff;
    }

    /**
     * Enables the first button and disables the second one.
     * */
    public void switchButtonsVisibility(MenuItem visibleB, MenuItem invisibleB) {
        visibleB.setVisible(true);
        visibleB.setEnabled(true);
        invisibleB.setEnabled(false);
        invisibleB.setVisible(false);
    }

    /**
     * Save the story without notification. Meant to be wrapped.
     * @return whether the story was successfully saved or not.
     * */
    private boolean saveStoryOnBackground() {
        String theTitle = String.valueOf(title.getText());
        String theText  = String.valueOf(texto.getText());

        Story story = new Story(theTitle, this.storyManager.getAuthorName(), theText);

        if(theTitle.matches("[a-zA-Z][a-zA-ZÀ-ú0-9_ \\-']*")){
            if(!isSaved()){
                cursorPosition = getCursorPosition();
                this.storyManager.saveStory(story, getCurrentFolder(), cursorPosition);
                setSaved(true);
            }
        }
        return isSaved();
    }

    private boolean autoSaveIsOn(){
        return sharedPref.getBoolean(getString(R.string.KEY_AUTOSAVE), true);
    }

    private int getCursorPosition() {
        return texto.getSelectionStart();
    }

    public void setSaved(boolean saved) {
        this.saved = saved;
    }

    public boolean isSaved() {
        return saved;
    }

    /**
     * Saves the current story
     * @param view the view that called this method
     * */
    public void saveStory(View view) {
        if(saveStoryOnBackground()){
            // Update save icon
            findViewById(R.id.fab).setAlpha(0.5f);
        }
        else {
            notifyInvalidTitleFormat();
        }
    }

    @NonNull
    protected File getCurrentFolder() {
        return new File(getHomeDirectory(), LAST_FOLDER);
    }

    public File getHomeDirectory() {
        return this.storyManager.getStoriesDirectory();
    }


    private void notifyInvalidTitleFormat() {
        //Notify invalid title format
        Log.w("User mistake", "Invalid Story name.");
        Toast.makeText(this, R.string.invalid_story_name, Toast.LENGTH_LONG).show();
    }

    public void openHTMLStyleSelectionDialog(MenuItem item){
        ChooseHTMLStyleDialogFragment dialog = new ChooseHTMLStyleDialogFragment();
        dialog.show(getSupportFragmentManager(), "ChooseHTMLStyleDialogFragment");
    }

    /**
     * Writes the text to an html file
     * @param style an HTMLStyle suitable for the HTMLTemplate
     * */
    public void exportTextAsHTML(HTMLStyle style) {

        String theTitle = String.valueOf(title.getText());
        String theText  = currentText();
        final File htmlStory;

        Story story = new Story(theTitle, this.storyManager.getAuthorName(), theText);

        if(theTitle.matches(getString(R.string.allowed_characters))){
            HTMLTemplate template = new HTMLTemplate(style);
            htmlStory = this.storyManager.exportAsHTML(theTitle, template.generate(story));
            //Notify on complete
            Snackbar.make(this.findViewById(R.id.fab), R.string.exported_HTML_story_notification, Snackbar.LENGTH_LONG)
                    .setAction(R.string.open, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            openExportedFile(htmlStory, "text/html");
                        }
                    }).show();
        }
        else {
            notifyInvalidTitleFormat();
        }
    }

    /**
     * Writes the text to a .txt file
     * */
    public void exportTextAsTXT(){
        String theTitle = String.valueOf(title.getText());
        String theText  = currentText();
        final File txtStory;

        if(theTitle.matches(getString(R.string.allowed_characters))){
            txtStory = this.storyManager.exportAsTXT(theTitle, this.storyManager.getAuthorName(), theText);
            //Notify on complete
            Snackbar.make(this.findViewById(R.id.fab), R.string.exported_story_notification, Snackbar.LENGTH_LONG)
                    .setAction(R.string.open, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            openExportedFile(txtStory, "text/text");
                        }
                    }).show();
        }
        else {
            notifyInvalidTitleFormat();
        }
    }

    /**
     * Shows a BottomSheet with the export options
     * */
    public void openExportOptions(MenuItem item){
        ExportOptionsDialogFragment bottomSheet = ExportOptionsDialogFragment.newInstance().setExporter(this);
        bottomSheet.show(getSupportFragmentManager(), "Export Options BS");
    }

    /**
     * Opens the exported file in an app choosen by the user.
     * */
    private void openExportedFile(File file, String mime){
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.fromFile(file), mime);
        startActivity(intent);
    }

    // Dialog Listener Interface
    @Override
    public void onDialogClick(HTMLStyle style) {
        this.exportTextAsHTML(style);
    }

    public String currentText(){
        return String.valueOf(texto.getText());
    }

    /**
     * Sets the story body to the state before the current text.
     * */
    public void undo() {
        textHistory.undo();
        if(textHistory.getCanUndo()){
            setUndoButtonEnable(true);
        }else{
            setUndoButtonEnable(false);
        }
    }

    public void redo() {
        textHistory.redo();
        if(textHistory.getCanRedo()){
            setRedoButtonEnable(true);
        }else{
            setRedoButtonEnable(false);
        }
    }

    // TextViewUndoRedo.Listener

    public void updateButtons() {
        invalidateOptionsMenu();
    }

    @Override
    public void setUndoButtonEnable(boolean state) {
            this.undoButtonIsOff = !state;
            updateButtons();
    }

    @Override
    public void setRedoButtonEnable(boolean state) {
            this.redoButtonIsOff = !state;
            updateButtons();
    }

    @Override
    public void updateSaveButton(){
        if(isSaved()){
            // Update save icon
            findViewById(R.id.fab).setAlpha(1.0f);
            setSaved(false);
        }
    }

    /** Get the word count and show it on the toolbar*/
    @Override
    public void updateWordCount(Editable s){

        String text = s.toString();
        String wordCount;

        String[] wordArray  = text.split("\\s+");
        if(wordArray[0].equals("")){
            wordCount = "0";
        }else {
            wordCount = String.valueOf(wordArray.length);
        }

        toolbar.setTitle(wordCount + " " + getString(R.string.word_count));
    }

    @Override
    public int getMaxTextSize() {
        return maxTextSize;
    }

    @Override
    public void setMaxTextSize(int maxTextSize) {
        this.maxTextSize = maxTextSize;
        texto.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxTextSize)});
    }

    public boolean isTextJustCreated(){
        return textJustCreated;
    }

    public void setTextJustCreated(boolean created){
        this.textJustCreated = created;
    }


    // ExportOptionsDialogFragment.Exporter
    @Override
    public void exportAsHTML() {
        ChooseHTMLStyleDialogFragment dialog = new ChooseHTMLStyleDialogFragment();
        dialog.show(getSupportFragmentManager(), "ChooseHTMLStyleDialogFragment");
    }

    @Override
    public void exportAsTEXT() {
        exportTextAsTXT();
    }

}
