package ar.com.nextgendesign.wattwriter.story;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ar.com.nextgendesign.wattwriter.R;


public class StoryAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<GridElement> storyArray;
    static final int STORY_TYPE     = 0;
    static final int FOLDER_TYPE    = 1;

    public StoryAdapter(Context context, ArrayList<GridElement> array){
        this.context = context;
        this.storyArray = array;
    }

    public void setStoryArray(ArrayList<GridElement> storyArray) {
        this.storyArray = storyArray;
    }

    // Stores references to the views
    private static class StoryViewHolder {
        TextView storyTitle;
        TextView  wordCount;
        TextView lastMod;
    }

    private static class FolderViewHolder {
        TextView folderName;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        GridElement element = storyArray.get(position);
        return element.getType();
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {

        int itemType = getItemViewType(position);

        switch(itemType){
            case STORY_TYPE:
                v = inflateStoryLayout(position, v, parent);
                break;
            case FOLDER_TYPE:
                v = inflateFolderLayout(position, v, parent);
        }

        return v;
    }

    @NonNull
    private View inflateStoryLayout(int position, View v, ViewGroup parent) {
        StoryViewHolder holder;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.story, parent, false);
            // Create the View Holder
            holder = new StoryViewHolder();
            holder.storyTitle     = (TextView) v.findViewById(R.id.story_title);
            holder.wordCount      = (TextView) v.findViewById(R.id.word_count);
            holder.lastMod        = (TextView) v.findViewById(R.id.last_modified);

            v.setTag(holder);
        }else {
            holder = (StoryViewHolder) v.getTag();
        }

        // Set TypeFace
        Typeface openSans = getTypeface();
        holder.storyTitle.setTypeface(openSans);
        // Assign the values
        final Story story       = (Story) getItem(position);
        holder.storyTitle.setText(story.getTitle());
        holder.wordCount.setText(story.getWordCount());
        holder.lastMod.setText(story.getLastModified());
        // Fade-in Animation
        addAnimation(v);
        return v;
    }

    private Typeface getTypeface() {
        return Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
    }

    @NonNull
    private View inflateFolderLayout(int position, View v, ViewGroup parent) {
        FolderViewHolder holder;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.folder, parent, false);
            // Create the View Holder
            holder = new FolderViewHolder();
            holder.folderName     = (TextView) v.findViewById(R.id.folder_name);

            v.setTag(holder);
        }else {
            holder = (FolderViewHolder) v.getTag();
        }

        // Set TypeFace
        Typeface openSans = getTypeface();
        holder.folderName.setTypeface(openSans);
        // Assign the values
        final Folder folder       = (Folder) getItem(position);
        holder.folderName.setText(folder.getTitle());
        // Fade-in Animation
        addAnimation(v);
        return v;
    }

    private void addAnimation(View v) {
        Animation anim = AnimationUtils.loadAnimation(context, R.anim.alpha);
        v.startAnimation(anim);
    }

    @Override
    public int getCount() {
        return this.storyArray.size();
    }

    @Override
    public GridElement getItem(int position) {
        return this.storyArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }


}
