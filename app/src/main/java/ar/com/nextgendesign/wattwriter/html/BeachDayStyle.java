package ar.com.nextgendesign.wattwriter.html;

public class BeachDayStyle implements HTMLStyle {
    @Override
    public String getStyle() {

        return "\n\t\tbody{\n" +
                "\t\t\tbackground-color: #faf2db;\n" +
                "\t\t\tfont-family: sans-serif;\n" +
                "\t\t\tfont-size: 16px;\n" +
                "\t\t}\n" +
                "\t\t#content{\n" +
                "\t\t\tmargin: 40px auto 20px;\n" +
                "\t\t\tcolor: #5E412F;\n" +
                "\t\t}\n" +
                "\t\th1{\n" +
                "\t\t\tborder-bottom: 2px solid #78C0A8;\n" +
                "\t\t\tcolor: #78C0A8;\n" +
                "\t\t\tpadding: 12px 12px 4px;\n" +
                "\t\t\ttext-align: center;\n" +
                "\t\t}\n" +
                "\t\tp{\n" +
                "\t\t\ttext-align: justify;\n" +
                "\t\t\twhite-space: pre-line;\n" +
                "\t\t\tpadding: 12px;\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t#author{\n" +
                "\t\t\tfont-size: 15px;\n" +
                "\t\t\ttext-align: center;\n" +
                "\t\t\tfont-style: italic;\n" +
                "\t\t\tmargin-top: -24px;\n" +
                "\t\t\tcolor: rgba(0,0,0,0.5);\n" +
                "\t\t}";
    }
}
