package ar.com.nextgendesign.wattwriter.story;


public class Story extends GridElement{
    private String wordCount;
    private String lastModified;
    private String author;
    private String content;

    /**
     * A Story as the grid representation understands it.
     * */
    public Story(String title, String wordCount, String author, String lM){
        this.title = title;
        this.wordCount = wordCount;
        this.author = author;
        this.lastModified = lM;
        this.content = "";
    }

    /**
     * A Story as the FileSystem understands it.
     * */
    public Story(String title, String author, String content){
        this.title = title;
        this.wordCount = "0";
        this.author = author;
        this.lastModified = "now";
        this.content = content;
    }

    public String getContent(){return content;}

    public String getWordCount() {
        return wordCount;
    }

    public String getAuthor() {
        return author;
    }

    public String getLastModified() {
        return lastModified;
    }

    @Override
    public void open(GridElement.Listener listener) {
        listener.openStory(title);
    }

    @Override
    public void delete(Listener listener) {
        listener.deleteStory(this.title);
    }

    @Override
    public int getType() {
        return 0;
    }
}
