package ar.com.nextgendesign.wattwriter.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import ar.com.nextgendesign.wattwriter.R;
import ar.com.nextgendesign.wattwriter.html.StyleDef;

/**
 * A simple {@link Fragment} subclass that represents a preview for an HTMLStyle
 */
public class StylePreviewFragment extends Fragment {
    private static String IMAGE_ID = "image_id";
    private static String STYLE_NAME = "style_name";

    public static StylePreviewFragment newInstance(StyleDef styleDef) {
        Bundle b = new Bundle();
        b.putInt(IMAGE_ID, styleDef.getResourceID());
        b.putString(STYLE_NAME, styleDef.getStyleName());
        StylePreviewFragment fragment = new StylePreviewFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.html_style, container, false);

        String name = getArguments().getString(STYLE_NAME);
        int imageID = getArguments().getInt(IMAGE_ID);

        FrameLayout styleContainer = (FrameLayout) v.findViewById(R.id.style_container);
        TextView styleName  = (TextView) v.findViewById(R.id.style_name);
        ImageView image     = (ImageView) v.findViewById(R.id.style_image);

        styleContainer.setBackgroundColor(Color.parseColor("#000000"));
        styleName.setText(name);
        styleName.setBackgroundColor(Color.parseColor("#000000"));
        image.setImageResource(imageID);

        return v;
    }


}
