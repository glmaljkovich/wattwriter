package ar.com.nextgendesign.wattwriter.db;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.core.v2.files.FileMetadata;

import java.util.concurrent.Semaphore;

import ar.com.nextgendesign.wattwriter.story.DropBoxStoryManager;

public class StoriesRepo {
    protected FragmentActivity activity;
    protected final Semaphore accessToStoriesDB = new Semaphore(0);
    protected SQLiteDatabase storiesDB;
    protected StoriesDBHelper dbHelper;

    public StoriesRepo(FragmentActivity act) {
        this.activity = act;
        CreateDatabaseTask dbCreator = new CreateDatabaseTask();
        dbCreator.execute();
    }

    // Blocks the process until the db is created in the constructor call
    public SQLiteDatabase getStoriesDB() {
        try {
            accessToStoriesDB.acquire();
            accessToStoriesDB.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return storiesDB;
    }

    /**
     * Must run on an Async Task
     * Not really a responsibility of the repo.
     * */
    public void deleteQueuedStories(DropBoxStoryManager manager){
        if(!storiesDB.isOpen()){
            storiesDB = dbHelper.getWritableDatabase();
        }
        // Get the stories marked to be deleted
        Cursor c = storiesDB.rawQuery(StoriesDB.QueuedActions.SELECT_STORIES_TO_DELETE, null);

        // Check there are, actually, said stories.
        if(c.getCount()>0){
            c.moveToFirst();
            int index = c.getColumnIndex(StoriesDB.QueuedActions.COL_STORY_NAME);
            do{
                String storyPath = c.getString(index);
                // if the story was succesfully deleted from dropbox, erase it from the database
                if(manager.deleteDropboxStory(storyPath)){

                    delete(StoriesDB.QueuedActions.COL_STORY_NAME, storyPath, StoriesDB.QueuedActions.TABLE_NAME);
                }
            }while (c.moveToNext());
            c.close();
        }
    }

    /**
     * Must run on an Async Task.
     * Not really a responsibility of the repo.
     * */
    public void saveQueuedStories(DropBoxStoryManager manager){
        if(!storiesDB.isOpen()){
            storiesDB = dbHelper.getWritableDatabase();
        }
        // Get the stories marked to be saved
        Cursor c = storiesDB.rawQuery(StoriesDB.QueuedActions.SELECT_STORIES_TO_SAVE, null);

        // Check there are, actually, said stories.
        if(c.getCount()>0){
            c.moveToFirst();
            int index = c.getColumnIndex(StoriesDB.QueuedActions.COL_STORY_NAME);
            do{
                String storyName = c.getString(index);
                // if the story was succesfully saved to dropbox, erase it from the database
                if(manager.saveFileToDropbox(storyName + ".txt")){

                    delete(StoriesDB.QueuedActions.COL_STORY_NAME, storyName, StoriesDB.QueuedActions.TABLE_NAME);
                }
            }while (c.moveToNext());
            c.close();
        }
    }


    /**
     * Saves a String value
     * */
    public void save(String storyNameColumn, String storyName, String valColumn,  String value, String tableName, String selectColumn){
        String WHERE  = storyNameColumn + " = ?";
        String[] args = {storyName};
        // The new Column Values
        ContentValues values    = new ContentValues();
        values.put(storyNameColumn, storyName);
        values.put(valColumn, value);
        Cursor c = storiesDB.rawQuery(selectColumn + " WHERE " + storyNameColumn + " = '" + storyName + "'", null);
        if(c.getCount() > 0){
            storiesDB.update(tableName, values, WHERE, args);
        }else{
            storiesDB.insert(tableName, null, values);
        }
    }

    /**
     * Saves an Int value
     * */
    public void save(String storyNameColumn, String storyName, String valColumn,  int value, String tableName, String selectColumn){
        String WHERE  = storyNameColumn + " = ?";
        String[] args = {storyName};
        // The new Column Values
        ContentValues values    = new ContentValues();
        values.put(storyNameColumn, storyName);
        values.put(valColumn, value);
        Cursor c = storiesDB.rawQuery(selectColumn + " WHERE " + storyNameColumn + " = '" + storyName + "'", null);
        if(c.getCount() > 0){
            storiesDB.update(tableName, values, WHERE, args);
        }else{
            storiesDB.insert(tableName, null, values);
        }
    }

    public void saveDropboxRev(String storyName, FileMetadata entry) {

        save(StoriesDB.DropboxRev.COL_STORY_NAME, storyName,
                StoriesDB.DropboxRev.COL_REV, entry.getRev(),
                StoriesDB.DropboxRev.TABLE_NAME, StoriesDB.DropboxRev.SELECT_REV_COL);
    }

    public void queueStoryAction(String storyName, String action) {

        save(StoriesDB.QueuedActions.COL_STORY_NAME, storyName,
                StoriesDB.QueuedActions.COL_ACTION, action,
                StoriesDB.QueuedActions.TABLE_NAME, StoriesDB.QueuedActions.SELECT_STORY_NAME_COL);
    }

    public void saveCursorPosition(String storyName, int position){
        save(StoriesDB.CursorPosition.COL_STORY_NAME, storyName,
                StoriesDB.CursorPosition.COL_CURSOR_POSITION, position,
                StoriesDB.CursorPosition.TABLE_NAME, StoriesDB.CursorPosition.SELECT_CURSOR_POSITION_COL);
    }


    public void deleteFromDropboxRev(String storyName) {

        delete(StoriesDB.DropboxRev.COL_STORY_NAME, storyName, StoriesDB.DropboxRev.TABLE_NAME);
    }

    public void deleteFromCursorPosition(String storyName) {

        delete(StoriesDB.CursorPosition.COL_STORY_NAME, storyName, StoriesDB.CursorPosition.TABLE_NAME);
    }


    public void delete(String storyNameColumn, String storyName, String tableName){
        String WHERE    = storyNameColumn + " = ?";
        String[] args   = {storyName};
        storiesDB.delete(tableName, WHERE, args);
    }

    public String getStoryRevision(String storyName) {
        return getString(StoriesDB.DropboxRev.COL_STORY_NAME, storyName, StoriesDB.DropboxRev.COL_REV, StoriesDB.DropboxRev.SELECT_REV_COL);
    }

    public String getString(String storyNameColumn, String storyName, String valColumn, String select){
        Cursor c = getStoriesDB().rawQuery(select + " WHERE " + storyNameColumn + " = '" + storyName + "'", null);
        String result = "0";
        if(c.getCount() > 0){
            c.moveToFirst();
            int index = c.getColumnIndex(valColumn);
            result = c.getString(index);
            c.close();
        }
        return result;
    }

    public int getInt(String storyNameColumn, String storyName, String valColumn, String select){
        Cursor c = getStoriesDB().rawQuery(select + " WHERE " + storyNameColumn + " = '" + storyName + "'", null);
        int result = 0;
        if(c.getCount() > 0){
            c.moveToFirst();
            int index = c.getColumnIndex(valColumn);
            result = c.getInt(index);
            c.close();
        }
        return result;
    }

    public int getCursorPosition(String storyName){
        return getInt(StoriesDB.CursorPosition.COL_STORY_NAME, storyName,
                StoriesDB.CursorPosition.COL_CURSOR_POSITION, StoriesDB.CursorPosition.SELECT_CURSOR_POSITION_COL);
    }


    public class CreateDatabaseTask extends AsyncTask<Void, Void, Void> {
        /** The system calls this to perform work in a worker thread and
         * delivers it the parameters given to AsyncTask.execute() */
        @Override
        protected Void doInBackground(Void... params) {
            dbHelper    = new StoriesDBHelper(activity.getApplicationContext());
            storiesDB   = dbHelper.getWritableDatabase();
            accessToStoriesDB.release();
            return null;
        }

    }
}
