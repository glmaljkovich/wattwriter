package ar.com.nextgendesign.wattwriter.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.core.android.Auth;

import java.io.File;

import ar.com.nextgendesign.wattwriter.R;
import ar.com.nextgendesign.wattwriter.fragments.AddFolderDialogFragment;
import ar.com.nextgendesign.wattwriter.fragments.DropboxStoryListFragment;
import ar.com.nextgendesign.wattwriter.fragments.LocalStoryListFragment;
import ar.com.nextgendesign.wattwriter.fragments.StoryListFragment;
import ar.com.nextgendesign.wattwriter.story.DropBoxStoryManager;

public class MainActivity extends AppCompatActivity implements AddFolderDialogFragment.Listener {

    public static final String STORY_CONTENT    = "ar.com.nextgendesign.wattwriter.STORY_CONTENT";
    public final static String STORY_TITLE      = "ar.com.nextgendesign.wattwriter.STORY_TITLE";
    public static final String STORY_SOURCE     = "ar.com.nextgendesign.wattwriter.STORY_SOURCE";
    public static final String LAST_OPENED_FOLDER = "ar.com.nextgendesign.wattwriter.LAST_OPENED_FOLDER";
    private static final int SETTINGS_INFO      = 1;
    private DrawerLayout drawer;
    private NavigationView drawerMenu;
    private ActionBarDrawerToggle mDrawerToggle;
    SharedPreferences sharedPref;
    StoryListFragment storyFragment;
    private boolean disconnectingFromDropbox = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Preferences
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        //Set the Drawer
        initializeDrawer(toolbar);

        // Set Author name
        setAuthorNameOnDrawerHeader();

        // Load Fragment
        if (savedInstanceState != null) { // saved instance state, fragment may exist
            // look up the instance that already exists by tag
            storyFragment = (StoryListFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        } else if (storyFragment == null) {
            // Create a new Fragment to be placed in the activity layout
            storyFragment = getLastOpenedStoryListFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            storyFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, storyFragment).commit();
        }

        // Animate the FAB
        animateTheFAB();
    }

    /**
     * Shows the FAB with an expanding animation
     * */
    private void animateTheFAB() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.scale);
        fab.startAnimation(anim);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SETTINGS_INFO){
            setAuthorNameOnDrawerHeader();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    public void onBackPressed() {
        if(storyFragment.canNavigateUp()){
            storyFragment.navigateUp();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // Get Token
        String token = getApplicationContext()
                .getSharedPreferences("DB_PREF", Context.MODE_PRIVATE)
                .getString("DB_TOKEN", "not set");
        // Show disconnect from dropbox button as enabled
        if(!token.equals("not set")){
            MenuItem disconnect = menu.findItem(R.id.disconnect_dropbox);
            disconnect.setEnabled(true);
        }
        // Handle navigateUp button
        //TODO figure out what the hell is going on that makes the activity null on dropbox logout
        if(isDisconnectingFromDropbox()){
            setDisconnectingFromDropbox(false);
        } else{
            handleNavigateUpButton(menu);
        }

        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here.
        switch (item.getItemId()){
            case R.id.add_folder:
                showAddFolderDialog();
                return true;
            case R.id.navigate_up:
                this.storyFragment.navigateUp();
                return true;
            default:
                // Check for drawer button
                return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onAddFolder(String name) {
        if(!name.matches(getString(R.string.allowed_characters))){
            notifyInvalidTitleFormat();
        } else {
            storyFragment.addFolder(name);
        }
    }

    private void notifyInvalidTitleFormat() {
        //Notify invalid title format
        Log.w("User mistake", "Invalid Folder name.");
        Toast.makeText(this, R.string.invalid_folder_name, Toast.LENGTH_LONG).show();
    }

    private void showAddFolderDialog() {
        AddFolderDialogFragment dialog = new AddFolderDialogFragment();
        dialog.show(getSupportFragmentManager(), "Add Folder Dialog");
    }

    private void handleNavigateUpButton(Menu menu) {
        MenuItem navigateUp     = menu.findItem(R.id.navigate_up);

        if(storyFragment.canNavigateUp()){
            navigateUp.setEnabled(true);
            navigateUp.setIcon(R.drawable.folder_back);
            navigateUp.getIcon().setAlpha(255);
        } else {
            navigateUp.setEnabled(false);
            navigateUp.setIcon(R.drawable.folder_back_black);
            navigateUp.getIcon().setAlpha(65);
        }


    }

    /**
     * Loads the last seen StoryListFragment
     * @return StoryListFragment
     * */
    private StoryListFragment getLastOpenedStoryListFragment() {

        String LastOpenedStoryList = sharedPref.getString(getString(R.string.LastStoryList), "local");

        if(LastOpenedStoryList.equals("local")){
            return new LocalStoryListFragment();
        }
        else{
            drawerMenu.setCheckedItem(R.id.dropbox);
            return new DropboxStoryListFragment();
        }
    }

    /**
     * Takes care of building and setting the drawerMenu
     * */
    private void initializeDrawer(final Toolbar toolbar) {
        // The drawer menu
        drawerMenu = (NavigationView) findViewById(R.id.drawer_menu);

        // Drawer Toggle -----------------------------------
        drawer          = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle   = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                drawer,         /* DrawerLayout object */
                toolbar,  /* the navbar */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                if(storyFragment != null){
                    setTitle(storyFragment.getTitle());
                }

            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                setTitle(R.string.action_settings);
            }
        };

        drawerMenu.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.story_list:
                        loadLocalStoryListFragment();
                        break;
                    case R.id.dropbox:
                        loadDropboxStoryListFragment();
                        break;
                    case R.id.settings:
                        Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivityForResult(intent, SETTINGS_INFO);
                        break;
                    case R.id.help:
                        openHelpActivity();
                }
                drawer.closeDrawers();
                return true;
            }
        });

        // Set the drawer toggle as the DrawerListener
        drawer.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.menu);
        // -------------------------------
    }

    /**
     * Sets the title on the toolbar.
     * PRECONDITION: The support action bar must be set.
     * */
    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    private void openHelpActivity(){
        Intent intent = new Intent(this, HelpActivity.class);
        startActivity(intent);
    }

    /**
     * Sets the author name on the drawer's header
     * PRECONDITION: Has to be called after initializeDrawer()
     * */
    private void setAuthorNameOnDrawerHeader() {
        TextView authorName = (TextView) drawerMenu.getHeaderView(0).findViewById(R.id.author_name);
        String authorString = sharedPref.getString(getString(R.string.KEY_AUTHOR_NAME), getString(R.string.author_name));
        authorName.setText(authorString);
    }

    /**
     * Starts the StoryEditorActivity
     * */
    public void openStoryEditor(View view) {
        Intent intent = new Intent(this,StoryEditorActivity.class);
        intent.putExtra(STORY_SOURCE, this.storyFragment.storySource());
        intent.putExtra(MainActivity.LAST_OPENED_FOLDER, this.storyFragment.getLastOpenedFolder());
        startActivity(intent);
    }

    public void loadDropboxStoryListFragment(){
        storyFragment.cancelRunningTask();
        storyFragment = new DropboxStoryListFragment();
        loadStoryListFragment(storyFragment);
    }

    public void loadLocalStoryListFragment(){
        storyFragment.cancelRunningTask();
        storyFragment = new LocalStoryListFragment();
        loadStoryListFragment(storyFragment);
    }

    /**
     * Replaces the content of the fragment_container with the fragment passed as parameter
     * */
    private void loadStoryListFragment(StoryListFragment storyFragment){
        // Add the fragment to the 'fragment_container' FrameLayout
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, storyFragment).commit();
    }

    public boolean isDisconnectingFromDropbox() {
        return disconnectingFromDropbox;
    }

    public void setDisconnectingFromDropbox(boolean disconnectingFromDropbox) {
        this.disconnectingFromDropbox = disconnectingFromDropbox;
    }

    /**
     * Resets the access token an takes the user to the Local Story List to avoid the Dropbox Login.
     * */
    public void disconnectFromDropbox(MenuItem item){
        // Delete the Session Token
        SharedPreferences.Editor editor = getApplicationContext().getSharedPreferences("DB_PREF", Context.MODE_PRIVATE).edit();
        editor.putString("DB_TOKEN", null).apply();

        //Delete the local files
        File storiesDirectory = new File(getExternalFilesDir(null).toString(), getString(R.string.dropbox_folder_name));
        this.storyFragment.deleteFolderWithAbsolutePath(storiesDirectory);

        // Reset the last opened folder
        new DropboxStoryListFragment().saveLastOpenedFolder("", true);

        // Swap fragment
        loadLocalStoryListFragment();
        drawerMenu.setCheckedItem(R.id.story_list);
        setDisconnectingFromDropbox(true);
        invalidateOptionsMenu();
    }

}
