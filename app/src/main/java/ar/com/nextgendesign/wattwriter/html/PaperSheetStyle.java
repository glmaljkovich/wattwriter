package ar.com.nextgendesign.wattwriter.html;


public class PaperSheetStyle implements HTMLStyle{
    @Override
    public String getStyle() {
        return "\n\t\tbody{\n" +
                "\t\t\tbackground-color: mediumturquoise;\n" +
                "\t\t\tbackground: dotted;\n" +
                "\t\t\tfont-family: serif;\n" +
                "\t\t\tfont-size: 17px;\n" +
                "\t\t}\n" +
                "\t\t#content{\n" +
                "\t\t\tmargin: 40px auto 20px;\n" +
                "\t\t\tcolor: #222222;\n" +
                "\t\t\tfont-family: serif;\n" +
                "\t\t\tbox-shadow: 1px 1px 6px #777777;\n" +
                "\t\t\tbackground-color: #ffffff;\n" +
                "\t\t}\n" +
                "\t\th1{\n" +
                "\t\t\tcolor: #333333;\n" +
                "\t\t\tpadding: 2em 12px 4px;\n" +
                "\t\t\ttext-align: center;\n" +
                "\t\t}\n" +
                "\n" +
                "\t\th1::after{\n" +
                "\t\t\twidth: 5em;\n" +
                "\t\t\theight: 2px;\n" +
                "\t\t\tdisplay: block;\n" +
                "\t\t\tmargin: 0 auto;\n" +
                "\t\t\tbackground-color: mediumturquoise;\n" +
                "\t\t\tcontent: \"\";\n" +
                "\t\t}\n" +
                "\t\tp{\n" +
                "\t\t\ttext-align: justify;\n" +
                "\t\t\twhite-space: pre-line;\n" +
                "\t\t\tpadding: 1em;\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t#author{\n" +
                "\t\t\tfont-size: 15px;\n" +
                "\t\t\ttext-align: center;\n" +
                "\t\t\tfont-style: italic;\n" +
                "\t\t\tmargin-top: -2em;\n" +
                "\t\t\tcolor: #666666;\n" +
                "\t\t}";
    }
}
