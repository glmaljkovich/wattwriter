package ar.com.nextgendesign.wattwriter.story;

import java.io.File;

/**
 * Created by gabriel on 26/08/16.
 */
public abstract class GridElement {
    protected String title;

    public String getTitle() {
        return title;
    }

    public long getId(){
        return this.title.hashCode();
    }

    public abstract void open(GridElement.Listener listener);

    public abstract void delete(GridElement.Listener listener);

    public abstract int getType();

    public interface Listener {
        void openStory(String title);
        void openFolder(String title, boolean isAbsolutePath);
        void deleteStory(String title);
        void deleteFolder(String title);
    }
}
