package ar.com.nextgendesign.wattwriter.html;


public class NovelStyle implements HTMLStyle {
    @Override
    public String getStyle() {
        return "\nbody{\n" +
                "\t\t\tfont-family: serif;\n" +
                "\t\t\tfont-size: 16px;\n" +
                "\t\t\tbackground: #ffffff;\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t.pure-g{\n" +
                "\t\t\tpadding-left: 6px;\n" +
                "\t\t\tpadding-right: 6px;\n" +
                "\t\t}\n" +
                "\t\t\n" +
                "\t\t#content{\n" +
                "\t\t\tpadding: 40px auto 20px;\n" +
                "\t\t\tcolor: #222222;\n" +
                "\t\t\tfont-family: serif;\n" +
                "\t\t}\n" +
                "\t\th1{\n" +
                "\t\t\tcolor: #333333;\n" +
                "\t\t\tfont-size: 2.2em;\n" +
                "\t\t\tpadding: 1.8em 12px 4px;\n" +
                "\t\t\ttext-align: center;\n" +
                "\t\t}\n" +
                "\n" +
                "\t\tp{\n" +
                "\t\t\ttext-align: justify;\n" +
                "\t\t\twhite-space: pre-line;\n" +
                "\t\t\tpadding: 1em;\n" +
                "\t\t}\n" +
                "\n" +
                "\t\tp:last-child::first-letter{\n" +
                "\t\t\tfont-size: 400%;\n" +
                "\t\t\tfloat: left;\n" +
                "\t\t\tpadding: 4px 8px 0 3px;\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t#author{\n" +
                "\t\t\tfont-size: 15px;\n" +
                "\t\t\tfont-style: italic;\n" +
                "\t\t\ttext-align: center;\n" +
                "\t\t\tmargin-top: -2.4em;\n" +
                "\t\t}\n";
    }
}
