package ar.com.nextgendesign.wattwriter.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import ar.com.nextgendesign.wattwriter.R;
import ar.com.nextgendesign.wattwriter.html.StylePreviewPagerAdapter;
import ar.com.nextgendesign.wattwriter.html.HTMLStyle;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChooseHTMLStyleDialogFragment extends DialogFragment {

    ViewPager vP;
    GestureDetector tapGestureDetector;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View stylePicker  = inflater.inflate(R.layout.style_picker, null);
        // Set the adapter
        vP     = (ViewPager) stylePicker.findViewById(R.id.pager);
        vP.setAdapter(new StylePreviewPagerAdapter(getChildFragmentManager()));
        vP.setClickable(true);

        tapGestureDetector = new GestureDetector(getContext(), new TapGestureListener());

        vP.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                tapGestureDetector.onTouchEvent(event);
                return false;
            }
        });

        // Set the layout
        return stylePicker;
    }

    public void click() {
        StylePreviewPagerAdapter adapter = (StylePreviewPagerAdapter) vP.getAdapter();
        HTMLStyle style = adapter.getStyle(vP.getCurrentItem());
        mListener.onDialogClick(style);
        dismiss();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
    }

    /* The activity that creates an instance of this dialog fragment must
            * implement this interface in order to receive event callbacks.
            * Each method passes the DialogFragment in case the host needs to query it. */
    public interface DialogListener {
        void onDialogClick(HTMLStyle style);
    }

    // Use this instance of the interface to deliver action events
    DialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (DialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    private class TapGestureListener extends GestureDetector.SimpleOnGestureListener{
        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            // Your Code here
            click();
            return true;
        }
    }
}
