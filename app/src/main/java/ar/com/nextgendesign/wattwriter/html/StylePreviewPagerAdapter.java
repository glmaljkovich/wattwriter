package ar.com.nextgendesign.wattwriter.html;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ar.com.nextgendesign.wattwriter.R;
import ar.com.nextgendesign.wattwriter.fragments.StylePreviewFragment;
import ar.com.nextgendesign.wattwriter.html.BeachDayStyle;
import ar.com.nextgendesign.wattwriter.html.HTMLStyle;
import ar.com.nextgendesign.wattwriter.html.PaperSheetStyle;
import ar.com.nextgendesign.wattwriter.html.StyleDef;

/**
 * A simple pager adapter that represents styles, in
 * sequence.
 */
public class StylePreviewPagerAdapter extends FragmentStatePagerAdapter {
    private StyleDef[] stylesArray = {
            new StyleDef("Beach Day", R.drawable.beach_day, new BeachDayStyle()),
            new StyleDef("Paper Sheet",R.drawable.paper_sheet, new PaperSheetStyle()),
            new StyleDef("Novel",R.drawable.novel, new NovelStyle())
    };

    public StylePreviewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return StylePreviewFragment.newInstance(stylesArray[position]);
    }

    @Override
    public int getCount() {
        return stylesArray.length;
    }

    public HTMLStyle getStyle(int position){
        return stylesArray[position].getStyle();
    }
}
