package ar.com.nextgendesign.wattwriter.story;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ar.com.nextgendesign.wattwriter.R;
import ar.com.nextgendesign.wattwriter.db.StoriesRepo;

/**
 * Manages stories in the file system
 */
public class StoryManager {

    protected FragmentActivity activity;
    protected StoriesRepo storiesRepo;

    public StoryManager(FragmentActivity act){
        this.activity    = act;
        this.storiesRepo = new StoriesRepo(act);
    }


    public class DeleteFolderTask extends AsyncTask<File, Void, Void> {
        /** The system calls this to perform work in a worker thread and
         * delivers it the parameters given to AsyncTask.execute() */
        @Override
        protected Void doInBackground(File... params) {
            deleteRecursively(params[0]);
            return null;
        }
    }


    @NonNull
    public File makeStoryFolder() {
        File storiesFolder = this.getStoriesDirectory();
        storiesFolder.mkdirs();
        Log.i("Stories Directory", storiesFolder.toString());
        return storiesFolder;
    }

    /**
     * Deletes a story from the specified directory
     * */
     public void deleteStoryFrom(String storyName, File directory){
        File story = new File(directory, storyName + ".txt");
        if (story.exists()) {
            story.delete();
            storiesRepo.deleteFromCursorPosition(storyName);
            Log.i("Story deleted", storyName);
        } else {
            Log.e("Error", story.toString() + " does not exist");
        }
    }

    /**
     * Open a particular story to be edited.
     * */
    public String openStory(String storyTitle, File storiesFolder) {
        // Create the files
        File theFile        = new File(storiesFolder, storyTitle + ".txt");

        // Capture the read content
        StringBuilder body      = new StringBuilder();
        BufferedReader reader   = null;

        //Create the buffer
        try {
            reader = new BufferedReader(new FileReader(theFile));
        } catch (FileNotFoundException e) { e.printStackTrace(); }

        //Read the file's content
        if(reader != null){
            try {
                String line;
                while ((line = reader.readLine()) != null) {
                    body.append(line).append("\n");
                }
                reader.close();
            } catch (IOException e) {e.printStackTrace();}
        }
        return body.toString();
    }

    /**
     * Returns the location of the stories directory depending on the available storage options
     * SD card is preferred over internal storage.
     * */
    @NonNull
    public File getStoriesDirectory() {
        File storiesFolder;
        if(isExternalStorageWritable()){
            storiesFolder = new File(Environment.getExternalStorageDirectory(), this.activity.getString(R.string.stories_folder_name));
        }
        else{
            storiesFolder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                    this.activity.getString(R.string.stories_folder_name));
        }

        return storiesFolder;
    }

    /**
     * Takes an array of filenames and returns a new one containing only the names of the .txt ones
     * @param files an array of filenames without extension
     * */
    public ArrayList<String> filterTextFiles(String[] files) {
        ArrayList<String> txtFiles = new ArrayList<>();

        for(String f : files){
            if(f.endsWith(".txt")){
                txtFiles.add(fileNameWithoutExtension(f));
            }
        }
        return txtFiles;
    }

    /**
     * It returns a filename without the .txt extension.
     * @param filename a .txt filename.
     * */
    public String fileNameWithoutExtension(String filename){

        return filename.substring(0, filename.length() - 4);
    }

    /**
     *  Checks if external storage is available for read and write
     * */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        return Environment.MEDIA_MOUNTED.equals(state);
    }

    /**
     * Writes a file to the Stories folder.
     * @param directory a Folder where to save the file
     * @param fileName a filename with it's corresponding extension.
     * @param body the file content */
    public File saveFile(File directory, String fileName, String body){
        File theFile = null;
        try
        {
            //Create the stories folder
            if(!directory.mkdirs()){
                if(!directory.exists()){
                    Log.e("ERROR", "Can't create folder in" + directory.toString() + ", mkdirs failed.");
                }
                else {
                    Log.i("Directory not created", "Already existed!");
                }
            }

            //Write to file
            theFile        = new File(directory, fileName);
            FileWriter writer   = new FileWriter(theFile);
            writer.append(body);
            writer.flush();
            writer.close();
            Log.i("File Saved", fileName);
        }
        catch(IOException e)
        {
            e.printStackTrace();
            String importError = e.getMessage();
            Log.e("Caught error", importError);
        }
        return theFile;
    }

    /**
     * Saves a story
     * */
    public void saveStory(Story story, File folder, int cursorPosition){
        saveFile(folder, story.getTitle() + ".txt", story.getContent());
        saveCursorPosition(folder.getPath() + "/" + story.getTitle(), cursorPosition);
    }

    public void saveCursorPosition(String storyPath, int pos){
        storiesRepo.saveCursorPosition(storyPath, pos);
    }

    /**
     * Saves a file with .html extension
     * @param storyName the name of the story
     * @param htmlCode the file's body
     * @return the File
     * */
    public File exportAsHTML(String storyName, String htmlCode){
        return saveFile(getExportDirectory(), storyName +".html", htmlCode);
    }

    /**
     * Saves a file with .txt extension
     * @param storyName the name of the story
     * @param body the file's body
     * @return the File
     * */
    public File exportAsTXT(String storyName, String author, String body){
        return saveFile(getExportDirectory(), storyName +".txt", storyName + "\n" + "by " + author + "\n\n" + body);
    }

    protected File getExportDirectory() {
        return new File(getStoriesDirectory(), "export");
    }

    /**
     * Returns the author name
     * */
    public String getAuthorName(){

        SharedPreferences preferences       = PreferenceManager.getDefaultSharedPreferences(activity);
        return preferences.getString(this.activity.getString(R.string.KEY_AUTHOR_NAME), this.activity.getString(R.string.author_name));
    }

    /**
     * A wrapper to get the last modified for a story
     * */
    private String getStoryLastModified(File folder, String storyTitle) {
        return getLastModified(new File(folder, storyTitle + ".txt"));
    }

    /**
     * Gets the last time a file was modified
     * */
    protected String getLastModified(File file) {
        String lastMod  = new SimpleDateFormat(activity.getString(R.string.date_format)).format(new Date(file.lastModified()));
        return lastMod;
    }


    /**
     * Deletes a folder and its contents.
     * Runs in background
     * */
    public void deleteFolder(File file){
        new DeleteFolderTask().execute(file);
    }

    /**
     * Deletes a file or folder recursively
     * PRECONDITION: Must run on an AsyncTask
     * */
    public void deleteRecursively(File file){
        if (file.isDirectory()) {
            for (File c : file.listFiles())
                deleteRecursively(c);
        }
        if (!file.delete())
            Log.e("Failed to delete file: ", file.getName());
    }

    /**
     * Moves a file or folder from one destination to another
     * @param origin the full path with name and extension (if it has one).
     * @param destination the full path with name and extension (if it has one).
     * PRECONDITION: Both paths are in the same mount point.
     * */
    public boolean moveFile(String origin, String destination){
        File source      = new File(origin);
        File endLocation = new File(destination);

        return source.renameTo(endLocation);
    }

    /**
     * @return the grid elements contained in the directory passed as parameter. Folders listed first.
     * */
    public ArrayList<GridElement> getGridElements(File storiesDirectory){
        ArrayList<GridElement> elements = new ArrayList<>();
        elements.addAll(getFoldersFrom(storiesDirectory));
        elements.addAll(getStoriesFrom(storiesDirectory));
        return elements;
    }

    /**
     * @return a list with the folders contained inside a directory.
     * */
    public ArrayList<Folder> getFoldersFrom(File storiesDirectory){
        ArrayList<Folder> folders = new ArrayList<>();
        for(File f : storiesDirectory.listFiles()){
            if(f.isDirectory()){
                Folder folder = new Folder(f.getName());
                folders.add(folder);
            }
        }
        return folders;
    }

    /**
     * gets the stories contained in the directory file passed as parameter
     * */
    protected ArrayList<Story> getStoriesFrom(File storiesDirectory) {
        ArrayList<Story> stories = new ArrayList<>();
        // For each storyTitle create a story.
        for(String storyTitle : filterTextFiles(storiesDirectory.list())){
            String wordCount = String.valueOf(this.openStory(storyTitle, storiesDirectory).split("\\s+").length);
            stories.add(new Story(storyTitle,
                    wordCount + " " + this.activity.getString(R.string.word_count),
                    getAuthorName(),
                    getStoryLastModified(storiesDirectory, storyTitle)));
        }
        return stories;
    }

    public int getCursorPosition(String storyName) {
        return storiesRepo.getCursorPosition(storyName);
    }
}
