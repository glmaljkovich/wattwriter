package ar.com.nextgendesign.wattwriter.db;

import android.provider.BaseColumns;

/**
 * Defines the contract for all the databases
 * */
public final class StoriesDB {

    /**
     * A table with two columns:
     * <b>StoryName</b> the story name.
     * <b>action</b> the action to be performed.
     * Precondition: There is just one row per Story. Story names are unique.
     * */
    public static abstract class QueuedActions implements BaseColumns{
        // Table specification
        public static final String TABLE_NAME       = "queuedActions";
        public static final String COL_STORY_NAME   = "storyName";
        public static final String COL_ACTION       = "action";

        // Values
        public static final String ACTION_DELETE_QUOTED = "'delete'";
        public static final String ACTION_SAVE_QUOTED   = "'save'";
        public static final String ACTION_DELETE        = "delete";
        public static final String ACTION_SAVE          = "save";

        // Queries
        public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                COL_STORY_NAME + " VARCHAR PRIMARY KEY, " + COL_ACTION + " VARCHAR" + ");";

        public static final String DELETE_TABLE  = "DROP TABLE IF EXISTS " + TABLE_NAME;

        public static final String SELECT_STORY_NAME_COL = "SELECT " + COL_STORY_NAME + " FROM " + TABLE_NAME;

        public static final String SELECT_STORIES_TO_DELETE  = SELECT_STORY_NAME_COL +
                " WHERE " + COL_ACTION + " =" + ACTION_DELETE_QUOTED;

        public static final String SELECT_STORIES_TO_SAVE  = SELECT_STORY_NAME_COL +
                " WHERE " + COL_ACTION + " =" + ACTION_SAVE_QUOTED;

    }

    /**
     * A table with two columns:
     * <b>storyName</b> the story name.
     * <b>rev</b> the action to be performed.
     * Precondition: There is just one row per Story. Story names are unique.
     * */
    public static abstract class DropboxRev implements BaseColumns{
        // Table specification
        public static final String TABLE_NAME       = "dropboxrev";
        public static final String COL_STORY_NAME   = "storyName";
        public static final String COL_REV          = "rev";

        // Queries
        public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                COL_STORY_NAME + " VARCHAR PRIMARY KEY, " + COL_REV + " VARCHAR" + ");";

        public static final String DELETE_TABLE  = "DROP TABLE IF EXISTS " + TABLE_NAME;

        public static final String SELECT_REV_COL = "SELECT " + COL_REV + " FROM " + TABLE_NAME;
    }

    /**
     * A table with two columns:
     * <b>storyName</b> the story name.
     * <b>cursor_pos</b> the cursor position.
     * Precondition: There is just one row per Story. Story names are unique.
     * */
    public static abstract class CursorPosition implements BaseColumns{
        // Table specification
        public static final String TABLE_NAME           = "cursorpos";
        public static final String COL_STORY_NAME       = "storyName";
        public static final String COL_CURSOR_POSITION  = "position";

        // Queries
        public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                COL_STORY_NAME + " VARCHAR PRIMARY KEY, " + COL_CURSOR_POSITION + " VARCHAR" + ");";

        public static final String DELETE_TABLE  = "DROP TABLE IF EXISTS " + TABLE_NAME;

        public static final String SELECT_CURSOR_POSITION_COL = "SELECT " + COL_CURSOR_POSITION + " FROM " + TABLE_NAME;
    }



}
