package ar.com.nextgendesign.wattwriter;


import org.junit.Before;
import org.junit.Test;

import java.lang.String;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UndoRedoTest {
    /*UndoRedo undoRedo;
    String text;
    String text2;
    String text3;
    TextState pText;
    TextState pText2;
    TextState pText3;
    UndoRedo.UndoRedoListener act = mock(UndoRedo.UndoRedoListener.class);

    @Before
    public void setUp(){
        undoRedo = new UndoRedo(10,act);
        text = "";
        text2 = "hola";
        text3 = "hola como";
        pText = new TextState(text, text2, 0, 0);
        pText2 = new TextState(text2, text3, 4, 4);
        pText3 = new TextState(text3, text2, 4, 0);
    }

    @Test
    public void aTextIsAddedToTheUndoStack(){
        when(act.undoButtonIsOff()).thenReturn(true);
        when(act.redoButtonIsOff()).thenReturn(true);
        undoRedo.addUndo(pText);
        assertEquals(undoRedo.undoStack.size(), 1);
    }

    @Test
    public void aTextIsCorrectlyRetrievedFromTheUndoStack(){
        when(act.undoButtonIsOff()).thenReturn(true);
        when(act.redoButtonIsOff()).thenReturn(true);
        undoRedo.addUndo(pText);
        assertEquals(undoRedo.undo(), pText);
    }

    @Test
    public void aTextIsCorrectlyAddedToTheRedoStack(){
        when(act.undoButtonIsOff()).thenReturn(true);
        when(act.redoButtonIsOff()).thenReturn(true);
        undoRedo.addRedo(pText);
        assertEquals(undoRedo.redoStack.size(), 1);
        assertEquals(undoRedo.redoStack.pop().textBefore, text);
    }

    @Test
    public void redoWorksAsExpected(){
        when(act.undoButtonIsOff()).thenReturn(true);
        when(act.redoButtonIsOff()).thenReturn(true);
        undoRedo.addRedo(pText);
        assertEquals(undoRedo.redo().textBefore, text);
    }

    @Test
    public void twoUndosGetStackedCorrectly(){
        when(act.undoButtonIsOff()).thenReturn(true);
        when(act.redoButtonIsOff()).thenReturn(true);
        undoRedo.addUndo(pText);
        undoRedo.addUndo(pText2);
        assertEquals(undoRedo.undo().textBefore, text2);
        assertEquals(undoRedo.undo().textBefore, text);
    }*/
}
