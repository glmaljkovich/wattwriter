package ar.com.nextgendesign.wattwriter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import ar.com.nextgendesign.wattwriter.activities.MainActivity;
import ar.com.nextgendesign.wattwriter.db.StoriesDB;
import ar.com.nextgendesign.wattwriter.db.StoriesDBHelper;
import ar.com.nextgendesign.wattwriter.story.DropBoxStoryManager;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by gabriel on 03/10/16.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.JELLY_BEAN)
public class DBTest {
    private DropBoxStoryManager storyManager;
    private String archivo = "hola.txt";
    private StoriesDBHelper dbHelper;
    private SQLiteDatabase db;
    private Context contexto;

    @Mock
    DropboxAPI<AndroidAuthSession> dbAPI;

    @Before
    public void setUp() throws Exception {
        contexto = Robolectric.setupActivity(MainActivity.class);
        dbHelper    = new StoriesDBHelper(contexto);
        db          = dbHelper.getWritableDatabase();
        //db.execSQL(StoriesDB.QueuedActions.DELETE_TABLE);
        //db.execSQL(StoriesDB.QueuedActions.CREATE_TABLE);

    }

/*    @After
    public void tearDown() throws Exception {
        db.close();
    }*/

    @Test
    public void testUnaStringSeSeparaCorrectamente() throws Exception{
        assertTrue(archivo.split("\\.")[0].equals("hola"));
    }

    @Test
    public void testLaBaseDeDatosSeCreaCorrectamente(){
        assertTrue(db.isOpen());
    }

    @Test
    public void testLasHistoriasAGuardarSeSeleccionanCorrectamente() throws Exception{
        // The new Column Values
        ContentValues values    = new ContentValues();
        values.put(StoriesDB.QueuedActions.COL_STORY_NAME, "Jose");
        values.put(StoriesDB.QueuedActions.COL_ACTION, StoriesDB.QueuedActions.ACTION_SAVE);
        db.insert(StoriesDB.QueuedActions.TABLE_NAME, null, values);
        Cursor c = db.rawQuery(StoriesDB.QueuedActions.SELECT_STORIES_TO_SAVE, null);
        assertEquals(c.getCount(), 1);
        c.close();
    }

    @Test
    public void testLasHistoriasABorrarSeSeleccionanCorrectamente() throws Exception{
        // The new Column Values
        ContentValues values    = new ContentValues();
        values.put(StoriesDB.QueuedActions.COL_STORY_NAME, "Pepito");
        values.put(StoriesDB.QueuedActions.COL_ACTION, StoriesDB.QueuedActions.ACTION_DELETE);
        db.insert(StoriesDB.QueuedActions.TABLE_NAME, null, values);
        Cursor c = db.rawQuery(StoriesDB.QueuedActions.SELECT_STORIES_TO_DELETE, null);
        Cursor c2 = db.rawQuery(StoriesDB.QueuedActions.SELECT_STORY_NAME_COL, null);
        assertEquals(c.getCount(), 1);
        assertEquals(c2.getCount(), 1);
        c.close();
        c2.close();
    }


}

