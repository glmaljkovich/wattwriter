package ar.com.nextgendesign.wattwriter;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.test.mock.MockContext;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ar.com.nextgendesign.wattwriter.db.StoriesDBHelper;
import ar.com.nextgendesign.wattwriter.db.StoriesRepo;
import ar.com.nextgendesign.wattwriter.story.StoryManager;

public class MyDropboxDatabaseTest {
    private StoryManager storyManager;
    private String archivo = "hola.txt";
    private StoriesDBHelper dbHelper;
    private SQLiteDatabase db;
    private Context contexto;
    private StoriesRepo storiesRepo;


    @Before
    public void setUp() throws Exception {
        contexto    = new MockContext();

    }

    @Test
    public void testUnaStringSeSeparaCorrectamente() throws Exception{
        Assert.assertTrue(archivo.split("\\.")[0].equals("hola"));
    }


    @Test
    public void testLaBaseDeDatosSeCreaCorrectamente(){
        dbHelper    = new StoriesDBHelper(contexto);
        db          = dbHelper.getWritableDatabase();
        Assert.assertNotNull(db);
    }


    /*@Test
    public void testLasHistoriasABorrarSeSeleccionanCorrectamente() throws Exception{
        dbHelper    = new StoriesDBHelper(contexto);
        db          = dbHelper.getWritableDatabase();
        //storiesRepo = new StoriesRepo();
    }*/
}


