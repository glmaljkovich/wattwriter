package ar.com.nextgendesign.wattwriter;

import org.junit.Test;

import java.io.File;

import ar.com.nextgendesign.wattwriter.fragments.LocalStoryListFragment;
import ar.com.nextgendesign.wattwriter.fragments.StoryListFragment;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class FileHierarchyTest {
    File f = new File("home");
    File f2;

    @Test
    public void fileGetsCreatedCorrectly(){
        f2 = new File(f, "");
        assertEquals(f2.getPath(), "home");
    }

    @Test
    public void pathGetsBuiltAsExpected(){
       f2 = new File(f, "stories");
        assertEquals("home/stories", f2.getPath());
    }

    @Test
    public void previousFolderPathGetsCalculatedCorrectly(){
        String previous = "";
        String current = "dropbox/stories";
        int lastSlash = current.lastIndexOf("/");
        if(lastSlash >=0){
            previous = current.substring(0, current.lastIndexOf("/"));
        }
        assertEquals("dropbox", previous);
    }

    @Test
    public void previousFolderForOnlyChildReturnsEmptyString(){
        String previous = "";
        String current = "dropbox";
        int lastSlash = current.lastIndexOf("/");
        if(lastSlash >=0){
            previous = current.substring(0, current.lastIndexOf("/"));
        }
        assertEquals("", previous);
    }

    @Test
    public void canNavigateUpForATwoLevelHierarchyReturnsTrue(){
        StoryListFragment frag = new LocalStoryListFragment();
        frag.saveLastOpenedFolder("dropbox/stories", true);

        assertTrue(frag.canNavigateUp());
    }

    @Test
    public void removeTheStoriesDirectoryFromTheFilePath(){
        File folder = new File("/dropbox/stories/level2/level3");
        String storiesDirectory = "/dropbox/stories";
        String relativeFolder = folder.getPath().replace(storiesDirectory, "");

        assertEquals("/level2/level3", relativeFolder);
    }

    @Test
    public void removeFileExtensionFromDottedPath(){
        String filePath  = "/storage/emulated/0/Android/data/ar.com.nextgendesign.wattwriter/files/dropbox/synced/i am groot.txt";
        String storyPath = filePath.substring(0, filePath.lastIndexOf("."));
        String expected  = "/storage/emulated/0/Android/data/ar.com.nextgendesign.wattwriter/files/dropbox/synced/i am groot";

        assertEquals(expected, storyPath);
    }
}