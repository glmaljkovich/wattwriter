package ar.com.nextgendesign.wattwriter;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;

import org.junit.runner.RunWith;
import org.mockito.Mock;

import ar.com.nextgendesign.wattwriter.db.StoriesDBHelper;
import ar.com.nextgendesign.wattwriter.story.DropBoxStoryManager;

import static ar.com.nextgendesign.wattwriter.db.StoriesDB.QueuedActions;


public class DropboxDatabaseTest extends AndroidTestCase{
    private DropBoxStoryManager storyManager;
    private String archivo = "hola.txt";
    private StoriesDBHelper dbHelper;
    private SQLiteDatabase db;
    private Context contexto;

    @Mock
    DropboxAPI<AndroidAuthSession> dbAPI;


    public void setUp() throws Exception {
        contexto = getContext();
        dbHelper    = new StoriesDBHelper(contexto);
        db          = dbHelper.getWritableDatabase();
        db.execSQL(QueuedActions.DELETE_TABLE);
        db.execSQL(QueuedActions.CREATE_TABLE);

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        db.close();
    }

    public void testUnaStringSeSeparaCorrectamente() throws Exception{
        assertTrue(archivo.split("\\.")[0].equals("hola"));
    }

    public void testLaBaseDeDatosSeCreaCorrectamente(){
        assertTrue(db.isOpen());
    }

    public void testLasHistoriasAGuardarSeSeleccionanCorrectamente() throws Exception{
        // The new Column Values
        ContentValues values    = new ContentValues();
        values.put(QueuedActions.COL_STORY_NAME, "Jose");
        values.put(QueuedActions.COL_ACTION, QueuedActions.ACTION_SAVE);
        db.insert(QueuedActions.TABLE_NAME, null, values);
        Cursor c = db.rawQuery(QueuedActions.SELECT_STORIES_TO_SAVE, null);
        assertEquals(c.getCount(), 1);
        c.close();
    }

    public void testLasHistoriasABorrarSeSeleccionanCorrectamente() throws Exception{
        // The new Column Values
        ContentValues values    = new ContentValues();
        values.put(QueuedActions.COL_STORY_NAME, "Pepito");
        values.put(QueuedActions.COL_ACTION, QueuedActions.ACTION_DELETE);
        db.insert(QueuedActions.TABLE_NAME, null, values);
        Cursor c = db.rawQuery(QueuedActions.SELECT_STORIES_TO_DELETE, null);
        Cursor c2 = db.rawQuery(QueuedActions.SELECT_STORY_NAME_COL, null);
        assertEquals(c.getCount(), 1);
        assertEquals(c2.getCount(), 1);
        c.close();
        c2.close();
    }


}
